package li.ues.topfriends.utils

import android.os.AsyncTask
import li.ues.topfriends.Utils
import org.json.JSONException
import org.json.JSONObject

open class CrawUserInfo : AsyncTask<UserData, Int, UserData?>() {

    override fun doInBackground(vararg params: UserData): UserData? {
        val data = params[0]
        var text = Utils.getUrl("https://web.facebook.com/chat/user_info/?ids[0]=" + data.id!!)

        if (text != null && text.contains(data.id!!)) {
            try {
                text = text.replace("for (;;);", "").replace("for(;;);", "")

                val json = JSONObject(text)
                val user = json
                    .getJSONObject("payload")
                    .getJSONObject("payload")
                    .getJSONObject("profiles")
                    .getJSONObject(data.id!!)

                val userName = user.getString("name")
                val userAlias = user.getString("vanity")
                val userImage = "https://graph.facebook.com/" + data.id + "/picture?width=256"

                if (userName != null && userAlias != null) {
                    data.name = userName
                    data.profilePicture = userImage
                    data.alias = userAlias
//                    data.name = "Sr. Gatinho"
//                    data.profilePicture = "https://placekitten.com/200/300"
                }

                return data
            } catch (e: JSONException) {
                println(e.message)
            }

        }
        return null
    }
}
