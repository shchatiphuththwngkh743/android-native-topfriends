package li.ues.topfriends

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.net.URLConnection

class FinalizadoActivity : GenericActivity() {
    internal var localImage: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (ContextCompat.checkSelfPermission(
                this@FinalizadoActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@FinalizadoActivity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                0
            )
        }

        setContentView(R.layout.activity_finalizado)

        val image = findViewById<View>(R.id.imagem) as ImageView
        val compartilhar = findViewById<View>(R.id.compartilhar) as Button
        val visualizar = findViewById<View>(R.id.visualizar) as Button
        val loading = findViewById<View>(R.id.loading) as LinearLayout
        val imagemLayout = findViewById<View>(R.id.imagem_layout) as ScrollView

        val bitmapPathObject = intent.getSerializableExtra("bitmap")

        if (bitmapPathObject == null) {
            Toast.makeText(
                applicationContext,
                getString(R.string.finalizar_generic_error),
                Toast.LENGTH_SHORT
            ).show()

            finish()
            return
        }

        val bitmapPath = bitmapPathObject as String

        val localImage = BitmapFactory.decodeFile(bitmapPath)
        // val localImage = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.size);

        compartilhar.isEnabled = false
        visualizar.isEnabled = false

        compartilhar.setOnClickListener {
            logAction("Apertou compartilhar")
            val share = Intent(Intent.ACTION_SEND)
            share.type = "image/jpeg"

            val values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, getString(R.string.finalizado_compartilhar))
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            val uri = contentResolver.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values
            )

            val outstream: OutputStream?
            try {
                outstream = contentResolver.openOutputStream(uri!!)
                localImage!!.compress(Bitmap.CompressFormat.JPEG, 100, outstream)
                outstream!!.close()
            } catch (e: Exception) {
                System.err.println(e.toString())
            }

            share.putExtra(Intent.EXTRA_STREAM, uri)
            startActivity(Intent.createChooser(share, getString(R.string.finalizado_compartilhar_imagem)))
        }

        visualizar.setOnClickListener {
            logAction("Apertou visualizar")
            val values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, getString(R.string.finalizado_visualizar))
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")

            val uri = contentResolver.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values
            )

            val outstream: OutputStream?
            try {
                outstream = contentResolver.openOutputStream(uri!!)
                localImage!!.compress(Bitmap.CompressFormat.JPEG, 100, outstream)
                outstream!!.close()
            } catch (e: Exception) {
                System.err.println(e.toString())
            }

            val view = Intent(Intent.ACTION_VIEW)
            view.setDataAndType(uri, "image/*")
            startActivity(view)
        }

        image.setImageBitmap(localImage)
        compartilhar.isEnabled = true
        visualizar.isEnabled = true
        loading.visibility = View.GONE
        imagemLayout.visibility = View.VISIBLE
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
