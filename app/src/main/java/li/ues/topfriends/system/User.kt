package li.ues.topfriends.system

import java.io.Serializable

/**
 * Created by schleumer on 12/01/16.
 */
class User : Serializable {
    var Id: String? = null
    var FbId: String? = null
    var ImageUrl: String? = null
    var Name: String? = null
    var ShortName: String? = null
}
